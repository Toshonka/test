# для примера взял latest, в докере не запускал
FROM node:latest

WORKDIR /app

COPY package.json .
COPY package-lock.json .

RUN npm i

COPY . .

ENV APP_TOKEN=TOKENTOKENTOKEN
ENV APP_PORT=3080

EXPOSE $APP_PORT

CMD ["node", "app.js"]