const express = require("express");
const fs = require("fs");
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
require('dotenv').config()
const app = express();

app.use(express.json())
app.listen(process.env.APP_PORT || 3080);
const appToken = process.env.APP_TOKEN || undefined

const csvWriter = createCsvWriter({
    // папку с файлом временно положил рядом
    path: 'data/data.csv',
    header: [
        { id: 'name', title: 'Name' },
        { id: 'age', title: 'Age' },
    ],
    append: true,
});

function auth(req, res, next) {
    if (req.headers.token !== appToken) {
        next(new Error('Empty token'));
    } else {
        next();
    }
  }

app.post("/set-name", auth, function(req, res){
    const { name, age } = req.body; // на типы не проверял и на выходе в гете не парсил, писал на чистом js
    if (!name || !age) {
      return res.status(400).json({ error: 'Empty name or age' });
    }
    
    csvWriter
      .writeRecords([{ name, age }])
      .then(() => {
        return res.json({ name, age });
      })
      .catch((error) => {
        console.error(error);
        return res.status(500).json({ error: 'Write error' });
      });
});


app.get("/get-name", auth, function(req, res){

    const { name } = req.query
    if (!name) {
        return res.status(400).json({ error: 'Empty name' });
      }

    const csv = fs.readFileSync('data/data.csv', 'utf-8');
    const rows = csv.split('\n');
    const data = rows.map((row) => row.split(','));
    const result = data.find((row) => {
        return row[0] === name
    })
    if (!result) {
        return res.status(404).json({ error: 'Name not found' });
    }
    return res.json({ name, age: result[1] });
});

